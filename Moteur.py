# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 14:00:35 2022

@author: fnac
"""
import xml.sax
import math
import re
from datetime import datetime


"""
Parser d'articles
"""
class PageContentHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.numPages = 0
        self.Titles = []
        self.Texts = []
        self.isInTitle = False
        self.isInText = False

    # Handle startElement
    def startElement(self, tagName, attrs):
        if tagName == 'title':
            self.isInTitle = True
        elif tagName == 'text':
            self.isInText = True
    
    # Handle endElement
    def endElement(self, tagName):
        if tagName == 'title':
            self.isInTitle = False
            self.numPages += 1
        if tagName == 'text':
            self.isInText = False
    
    # Handle text data
    def characters(self, chars):
        if self.isInTitle:
            self.Titles.append(chars)
            self.Texts.append("")
        elif self.isInText:
            self.Texts[self.numPages-1]+=chars
      
    # Handle startDocument
    def startDocument(self):
        print('Ouverture')

    # Handle endDocument
    def endDocument(self):
        print('Fin de traitement')   
 
"""
Parser Mot Page
"""
class RMPContentHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.rmp = {}
        self.isInWord = False
        self.isInPage = False
        self.currWord = ""
        self.currAttr = ""
        self.stock = ""

    # Handle startElement
    def startElement(self, tagName, attrs):
        if tagName == 'mot':
            self.currWord = attrs.get("id")
            self.rmp[self.currWord]={}
            self.isInWord = True
        elif tagName == 'page':
            self.currAttr = attrs.get("id")
            self.isInPage = True
    
    # Handle endElement
    def endElement(self, tagName):
        if tagName == 'mot':
            self.isInWord = False
        if tagName == 'page':
            self.isInPage = False
    
    # Handle text data
    def characters(self, chars):          
        if self.isInPage:
            if len(chars)<=6 and chars[0]=="0" and len(chars)>2 and chars[1]==".":
                self.stock = chars
            elif float(chars)>1:
                # print("Stock ="+self.stock)
                # print("Chars ="+chars)
                
                if(len(self.stock)==0):
                    self.stock="0."
                # print("Stock+Chars ="+self.stock+chars)
                # print("\n")
                self.rmp[self.currWord][int(self.currAttr)]= float(self.stock+chars)
                self.stock=""
            else:
                self.rmp[self.currWord][int(self.currAttr)]= float(chars)
            
      
    # Handle startDocument
    def startDocument(self):
        print('Ouverture RMP')

    # Handle endDocument
    def endDocument(self):
        print('Fin de traitement RMP')   
 

"""
Dictionnaire
"""
class Dictionnaire():
    def __init__(self):
        self.MotsIdF = {}

    def getDictionnary(self):
        with open('dico20000.txt', encoding="UTF-8") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                (a, b) = lines[i].split(":")
                self.MotsIdF[a]=float(b)

"""
Chercheur d'Article dans le sommaire
"""
class Bibliotheque():
    def __init__(self):
        self.fichiers = []
        self.premiers = []
        self.derniers = []
        self.allTitles = {}
     # Récupère le sommaire   
    def getSommaire(self):
        with open('sommaire.txt', encoding="UTF-8") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                (a, b) = lines[i].split(":")
                self.fichiers.append(a)
                (c, d) = b.split("-")
                self.premiers.append(int(c))
                self.derniers.append(int(d))
    
    def getAllTitles(self):
        with open('dictionnaire.txt', encoding="UTF-8") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                c = lines[i].index(":")
                a = int(lines[i][:c])
                b = lines[i][c+1:-1]
                self.allTitles[b]=a
    
    # Récupère l'emplacement d'un article précis dans le sommaire de fichiers    
    def getArticleFile(self, n):
        for i in range(len(self.fichiers)):
            if(n>=self.premiers[i] and n<=self.derniers[i]):
                return self.fichiers[i]
        return -1
    # Récupère tous les articles d'un fichier donnée
    def getArticles(self, page):
      handler = PageContentHandler()
      parser = xml.sax.make_parser()
      parser.setFeature(xml.sax.handler.feature_namespaces, 0)
      parser.setContentHandler( handler )
      parser.parse(page)
      articles = []
      pos = self.positionInS(page)
      for i in range(len(handler.Titles)):
          a = Article(handler.Titles[i], handler.Texts[i], i-self.premiers[pos], i+self.premiers[pos])
          articles.append(a)
      return articles
    
    # Récupère la position de la page dans le sommaire
    def positionInS(self, page):
        for i in range(len(self.fichiers)):
            if (self.fichiers[i]==page):
                return i
    
    # Récupère l'article précis dans l'ensemble des articles
    def getArticleN(self, n):
        page = self.getArticleFile(n)
        pos = self.positionInS(page)
        return self.getArticles(page)[n-self.premiers[pos]]
    
    def getLinks(self, page):
        print("Inside")
        linkRegex = re.compile('[[[]{2}(.*)[\]]{2}')
        textRegex = re.compile('[\w\$\&\+\,\:\;\=\?\@\#\|\'\<\>\.\-\^\*\(\)\%\!\ ]+')
        links = []
        points = []
        print(linkRegex.findall(page))
        for i in set(linkRegex.findall(page)):
            links.append(textRegex.findall(i)[0])
            print("Dans Boucle 1")
            print(links)
        for i in links:
            print(i)
            print(self.allTitles[i])
            if i in self.allTitles:
                points.append(self.allTitles[i])
            print(points)
        print(points)
    
    
    
        
"""
Article
"""
class Article():
     def __init__(self, titre, texte, numLocal, numGlobal):
        self.Title = titre
        self.Text = texte
        self.NumeroLocal= numLocal # Position dans le fichier
        self.NumeroGlobal = numGlobal # Position dans l'ensemble des fichiers
        self.AllLinks = [] # Liste des pages liées
    
    # Récupère le titre de l'article
     def getTitle(self):
        return self.Title
    # Récupère le texte de l'article
     def getText(self):
        return self.Text
    
    # Récupère la liste des liens d'un article
     def findLinks(self):
        linkRegex = re.compile('[[]{2}[^\]]*[\]]{2}')
        goodPart = re.compile('((.*)\|(.*))')
        links = linkRegex.findall(self.Text)
        all = []
        for c in links:
            mot = str(c)[2:-2]
            #print(mot)
            if mot.__contains__("|"):
                all.append((goodPart.match(mot)).group(2))
            else:
                all.append(mot)
        return set(all)
    
     def getAllLinks(self, allTitles):
         for titre in self.findLinks():
             if titre in allTitles:
                 self.AllLinks.append(allTitles[titre])

"""
Matrice
"""
def Matrice():
    def __init__(self):
        print("Matrice")
    
    
    

"""
Relation Mot Page
"""


class RelationMotPages():
    def __init__(self, mots):
        self.MotsPages = {}
        for i in range(len(mots)):
            self.MotsPages[mots[i]] = {}

    def createRelation(self, article):
        mots =  article.Text.split(' ')
        mots = set(mots)
        for  m in mots:
            if m in self.MotsPages:
                    self.MotsPages[m][article.NumeroGlobal]= article.Text.count(m)
    
    def TF(self, mot, article):
        if mot in article.Text:
            return 1+math.log(self.MotsPages[mot][article.NumeroGlobal])
        else:
            return 0
    
    def Nd(self, article):
        somme = 0
        for m in self.MotsPages:
            if article.NumeroGlobal in self.MotsPages[m]:
                somme += self.TF(m, article)**2
        return math.sqrt(somme)
    
    def TFNd(self, article):
        for mot in self.MotsPages:
            if article.NumeroGlobal in self.MotsPages[mot]:
                self.MotsPages[mot][article.NumeroGlobal]=self.TF(mot, article)/self.Nd(article)
    
    
    def MakeRMP(self, article):
        self.createRelation(article)
        self.TFNd(article)
   
      # Enregistre la relation Mot Page dans un fichier
    def save(self, k):
        f = open("RelationMotsPages"+str(k)+".xml", "w")
        i = 0
        keys = list(self.MotsPages.keys())
        f.write("<root>\n")
        for m in self.MotsPages:
            s = "  <mot id=\"" + m +"\">\n"
            for p in self.MotsPages[keys[i]]:
                s+= "    <page id=\""+str(p) + "\">" + str(self.MotsPages[keys[i]][p]) + "</page>\n"
            i+=1
            s+= "  </mot>\n"
            f.write(s)
        f.write("</root>")
        print("It's saved")
        f.close()       
    
    # Efface les pages les moins pertinentes
    def EPMP(self, mot, k):
        self.MotsPages[mot]={key : val for key, val in self.MotsPages[mot].items() if not (isinstance(val, float) and (val < k))}
    
     # Récupère 
    def getRMP(self, page):
      handler = RMPContentHandler()
      parser = xml.sax.make_parser()
      parser.setFeature(xml.sax.handler.feature_namespaces, 0)
      parser.setContentHandler( handler )
      parser.parse(page)
      return handler.rmp
      
    
    def getFinalRMP(self, dictionnaire):
        dict1=self.getRMP("RelationMotsPages0.xml")
        print("Enter Main For")
        for i in range(1, 21):
            print("Enter "+ str(i))
            dict2= self.getRMP("RelationMotsPages"+str(i)+".xml")
            k1 = list(dict1.keys())
            k2 = list(dict2.keys())
            for k in k2:
                c = list(dict2[k].keys())
                if(k not in k1):
                    dict1[k]={}
                    for l in c:
                        dict1[k][l]=dict2[k][l]
                        
            print("Merge completed")
        print("Enter second loop")
        self.MotsPages=dict1
        for mot in dict1:
              for page in dict1[mot]:
                  self.MotsPages[mot][page]=self.MotsPages[mot][page]*dictionnaire.MotsIdF[mot]
              self.EPMP(mot, 0.09)
        self.save(100)

class RMP():
    def __init__(self, mots):
        self.MotsPages = {}
        for i in range(len(mots)):
            self.MotsPages[mots[i]] = {}
    
    # Prend une papge en paramètre, son identifiant et le dictionnaire
    def MakeRMP(self, page, idP, dictio):
        mots = page.split(' ')
        for m in mots:
            if m in self.MotsPages:
                if idP in self.MotsPages[m]:
                    (self.MotsPages[m])[idP]+=1
                else:
                    (self.MotsPages[m])[idP]=1
                self.TF(m)
                self.TFNdIdf(m, idP, dictio)
                
    # Calcul le TF de chaque mot dans chaque page
    def TF(self, mot):
        for p in self.MotsPages[mot]:
            if(self.MotsPages[mot][p]>=0):
                self.MotsPages[mot][p]= 1+math.log10(self.MotsPages[mot][p])
            #print(self.MotsPages[mot][p])
    
    # Calcul le Nd de chaque page
    def Nd(self, idP):
        somme = 0
        for m in self.MotsPages:
            if idP in self.MotsPages[m]:
                somme+= (self.MotsPages[m][idP])**2
        return somme
   
     
    # Calcule et intègre à chaque une page sa relation Idf x TF/ND
    def TFNdIdf(self, mot, idP, dictio):
        if idP in self.MotsPages[mot]:
            self.MotsPages[mot][idP]= (dictio[mot]*self.MotsPages[mot][idP])/self.Nd(idP)
    
    
    def PagePertinente(self, mot):
        return max(self.MotsPages[mot], key=self.MotsPages[mot].get)
    
    # Efface les pages les moins pertinentes
    def EPMP(self, mot, k):
        self.MotsPages[mot]={key : val for key, val in self.MotsPages[mot].items() if not (isinstance(val, int) and (val < k))}
    
    # Enregistre la relation Mot Page dans un fichier
    def save(self):
        f = open("RelationMotsPages.xml", "w")
        i = 0
        keys = list(self.MotsPages.keys())
        f.write("<root>\n")
        for m in self.MotsPages:
            s = "<mot>" + m
            for p in self.MotsPages[keys[i]]:
                s+= "<page id="+str(p) + ">" + str(self.MotsPages[keys[i]][p]) + "</page>\n"
            i+=1
            s+= "</mot>\n"
            f.write(s)
        f.write("</root>")
        print("It's saved")
        f.close()    
        
    
        


# 1 Charger le dictionnanire

dictio = Dictionnaire()
dictio.getDictionnary()


# 2 Ouvrir la bibliothèque et obtenir le sommaire

library = Bibliotheque()
library.getSommaire()

library.getAllTitles()


def links():
    f = open("Matrice.txt", "w")
    x = 0
    for i in range(len(library.fichiers)):
        print("Demarrage de "+ str(i))
        articles = library.getArticles(library.fichiers[i])
        for article in articles :
            article.getAllLinks(library.allTitles)
            s = str(article.NumeroGlobal) + " : " + str(article.AllLinks) + "\n"
            x=x+1
            print(str(int((x*100)/171181))+"%")
            f.write(s)
    f.close()

#links()

# 3 Ouvrir le gestionnaire de relation Mot Page

rmp = RelationMotPages(list(dictio.MotsIdF.keys()))


start_time =datetime.now()

def cv(i):
    print("Demarrage de "+ str(i))
    print(library.fichiers[i])
    articles = library.getArticles(library.fichiers[i])
    n = 0
    for article in articles :
        rmp.MakeRMP(article)
        print(article.getTitle() +" "+ str(int((n*100)/len(articles))) + "%" )
        n=n+1
    rmp.save(i)
    end_time = datetime.now()
    print('Duration : {}'.format(end_time-start_time))
# cv(0)

print("début choix")
rmp.getFinalRMP(dictio)
end_time = datetime.now()
print('Duration : {}'.format(end_time-start_time))

print("fin choix")
"""
for i in list(dictio.MotsIdF.keys()):
    rmp.EPMP(i, 0.5)
"""
#rmp.save()
#rmp.getRMP("RelationMotsPages.xml")







    




















