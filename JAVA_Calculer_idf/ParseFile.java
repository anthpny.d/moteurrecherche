import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseFile {

	public static DefaultHandler handler;
	public static SAXParser saxParser;
	public static HashMap<String,Integer> dico;
	public static TreeSet<String> stopword;
	public static TreeSet<String> set;
	public static void ParseFileInit() {
		dico=new HashMap<>();
		stopword=new TreeSet<>();
		try {
			String strCurrentLine;
			BufferedReader bufferedreader;
			bufferedreader = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/stop_words_french.txt"));
			while ((strCurrentLine = bufferedreader.readLine()) != null) {
				stopword.add(strCurrentLine);
			}
			bufferedreader.close();
			bufferedreader = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/stop_words_english.txt"));
			while ((strCurrentLine = bufferedreader.readLine()) != null) {
				stopword.add(strCurrentLine);
			}
			bufferedreader.close();
			//Obtenir la configuration du sax parser
			SAXParserFactory spfactory = SAXParserFactory.newInstance();
			//Obtenir une instance de l'objet parser
			saxParser = spfactory.newSAXParser();
			handler = new DefaultHandler() {

				public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{
					if (qName.equalsIgnoreCase("title")) {
						set=new TreeSet<>();
					}
				}

				public void endElement(String uri, String localName,String qName) throws SAXException {}

				public void characters(char ch[], int start,int length) throws SAXException {
					String s=new String(ch, start, length);
					s=s.replaceAll("\\p{Punct}", " ");
					Pattern p = Pattern.compile("\\d",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
					s=p.matcher(s).replaceAll(" ");
					String []tab=s.split(" ");
					int nbOcc;
					for(String mot : tab) {
						mot=mot.replace(" ","")
								.replace("\n","")
								.replace("\t","")
								.replace("é","e")
								.replace("è","e")
								.replace("ê","e")
								.replace("à","a")
								.replace("ù","u")
								.replace("ç","c")
								.replaceAll("\\p{Punct}", "")
								.toLowerCase();
						p = Pattern.compile("[^a-z]",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
						mot=p.matcher(mot).replaceAll("");
						nbOcc=0;
						if(mot.length()>2 && !stopword.contains(mot)) {
							if(set.add(mot)) {
								if(dico.containsKey(mot)) {
									nbOcc=dico.get(mot);
								}
								nbOcc++;
								dico.put(mot,nbOcc);
							}
						}
					}
				}

			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void parse(Path p) {
		try {
			if(!p.getFileName().toString().equals("output")){
				System.out.println("Début : "+p.getFileName());
				saxParser.parse(new FileInputStream(p.toString()), handler);
				System.out.println("Fin : "+p.getFileName());
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
