import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		ParseFile.ParseFileInit();
		try (Stream<Path> paths = Files.walk(Paths.get("/home/dupre/Programmation/M2/AccesNumerique/TP1/output"))) {
			paths.forEach(ParseFile::parse);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<Entry<String, Integer>> entrySet = ParseFile.dico.entrySet();

		List<Entry<String, Integer>> list = new ArrayList<>(entrySet);

		Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
		try {
			File file = new File("/home/dupre/Programmation/M2/AccesNumerique/TP1/dico.txt");
			if(!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i=0;i<50000;i++) {
				//bw.write(list.get(i).getKey()+":"+list.get(i).getValue()+"\n");
				bw.write(list.get(i).getKey()+":"+Math.log10(171181/list.get(i).getValue().doubleValue())+"\n");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
