# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 13:48:15 2022

@author: fnac
"""

import xml.sax
import string
from xml.sax.saxutils import escape
import xml.etree.cElementTree as ET
import xml.dom.minidom



# define a Page ContentHandler class that extends ContenHandler
class PageContentHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.numPages = 0
        self.Titles = []
        self.Texts = []
        self.isInTitle = False
        self.isInText = False

    # Handle startElement
    def startElement(self, tagName, attrs):
        if tagName == 'title':
            self.isInTitle = True
        elif tagName == 'text':
            self.isInText = True
    
    # Handle endElement
    def endElement(self, tagName):
        if tagName == 'title':
            self.isInTitle = False
            self.numPages += 1
        if tagName == 'text':
            self.isInText = False
    
    # Handle text data
    def characters(self, chars):
        if self.isInTitle:
            self.Titles.append(chars)
            self.Texts.append("")
        elif self.isInText:
            self.Texts[self.numPages-1]+=chars
      
    # Handle startDocument
    def startDocument(self):
        print('Ouverture')

    # Handle endDocument
    def endDocument(self):
        print('Fin de traitement')   


def ourCorpus(Titles, Texts, Keywords):
    corpus = []
    for x in range(len(Titles)):
        if any(map(Texts[x].__contains__, Keywords)):
            corpus.append((Titles[x], Texts[x]))
    print("Corpus crée")
    return corpus


def fileGenerator(xmlFile, corpus):
    inner_template = string.Template('    <title>${value1}</title>\n<text>${value2}</text>\n')
    outer_template = string.Template("""<root>
 <page>
${document_list}
 </page>
</root>
 """)
    inner_contents = [inner_template.substitute(value1=escape(value1), value2=escape(value2)) for (value1, value2) in corpus]
    result = outer_template.substitute(document_list='\n'.join(inner_contents))
    f = open(xmlFile, "a", encoding='utf-8')
    f.write(result)
    f.close()
    print("Fichier crée")
 
 
            
def main():
      # create a new content handler for the SAX parser
      handler = PageContentHandler()
      
      # create an XMLReader
      parser = xml.sax.make_parser()
      # turn off namepsaces
      parser.setFeature(xml.sax.handler.feature_namespaces, 0)
   
      parser.setContentHandler( handler )
      parser.parse("xae.xml")
      
      #paradis = ["spirale", "scarabée", "ruine", "figue", "dolorosa", "singularité", "ange", "hortensias", "empereur"]
      paradis = ["science", "math", "chimie", "physique", "biologie"]
      corpus = ourCorpus(handler.Titles, handler.Texts, paradis)
      print(len(corpus))
      #xmlMaker(corpus)
      fileGenerator("articleX5.xml", corpus)
      

if __name__ == '__main__':
    main()