import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Balise {

	public String tag;
	public String content;
	public static ArrayList<String> stopword;
	
	public static void readStopWord() {
		stopword=new ArrayList<>();
		try {
			String strCurrentLine;
			BufferedReader bufferedreader;
			bufferedreader = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/stop_words_french.txt"));
			while ((strCurrentLine = bufferedreader.readLine()) != null) {
				stopword.add(strCurrentLine);
			}
			bufferedreader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Balise() {
		this.tag="";
		this.content="";
	}

	public void changeBalise(String tag,String content) {
		this.tag=tag;
		this.content=content;
	}

	public void modifyContent(String c) {
		this.content+=c;
	}

	public void cleanContent() {
		Pattern p = Pattern.compile("\\{\\{.+?}}",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p2 = Pattern.compile("\\[http.+?]",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p3 = Pattern.compile("\\[\\[\\d*]]",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p4 = Pattern.compile("\\[File.+?]",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p5 = Pattern.compile("\\[Fichier.+?]",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p6 = Pattern.compile("== Notes et références ==.*",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p7 = Pattern.compile("==.+?==",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p8 = Pattern.compile("== Annexes ==.*",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p9 = Pattern.compile("=== Liens externes ===.*",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p10 = Pattern.compile("=== Bibliographie ===.*",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Pattern p11 = Pattern.compile("== Voir aussi ==.*",Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		this.content=p.matcher(this.content).replaceAll(" ");
		this.content=p4.matcher(this.content).replaceAll(" ");
		this.content=p5.matcher(this.content).replaceAll(" ");
		ArrayList<String> listeLien=new ArrayList<>();
		Matcher m = Pattern.compile("\\[\\[.+?]]")
				.matcher(this.content);
		int i=0;
		String s;
		while (m.find()) {
			s=m.group();
			listeLien.add(s);
			this.content=this.content.replace(s,"lienremplace"+i);
			i++;
		}
		this.content=p2.matcher(this.content).replaceAll(" ");
		this.content=p3.matcher(this.content).replaceAll(" ");
		this.content=p6.matcher(this.content).replaceAll(" ");
		this.content=p7.matcher(this.content).replaceAll(" ");
		this.content=p8.matcher(this.content).replaceAll(" ");
		this.content=p9.matcher(this.content).replaceAll(" ");
		this.content=p10.matcher(this.content).replaceAll(" ");
		this.content=p11.matcher(this.content).replaceAll(" ");
		
		this.content=this.content.toLowerCase();
		this.content = this.content.replaceAll("\\p{Punct}", " ");
		
		for(String stopw:stopword) {
			stopw=stopw.replace("\n","");
			stopw=" "+stopw+" ";
			this.content=this.content.replace(stopw, " ");
		}
		for(int j=0;j<listeLien.size();j++) {
			this.content=this.content.replace("lienremplace"+j,listeLien.get(j));	
		}
		this.eraseSpace();
	}

	public void eraseSpace() {
		this.content=this.content.replace('\n', ' ');
		this.content=this.content.trim().replaceAll(" +", " ");
	}


	public String getTag() {
		return tag;
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "<"+this.tag+">"+this.content+"</"+this.tag+">";
	}


}
