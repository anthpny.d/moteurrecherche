import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main{

	public static void main(String[] args) {
		ParseFile.ParseFileInit();
		Balise.readStopWord();
		try (Stream<Path> paths = Files.walk(Paths.get("/home/dupre/Programmation/M2/AccesNumerique/TP1/articleX/articleX2.xml"))) {
			paths.forEach(ParseFile::parse);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}