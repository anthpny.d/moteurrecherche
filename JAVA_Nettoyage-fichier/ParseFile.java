import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseFile {

	public static DefaultHandler handler;
	public static SAXParser saxParser;
	public static void ParseFileInit() {
		try {
			//Obtenir la configuration du sax parser
			SAXParserFactory spfactory = SAXParserFactory.newInstance();
			//Obtenir une instance de l'objet parser
			saxParser = spfactory.newSAXParser();
			handler = new DefaultHandler() {

				boolean btitle = false;
				boolean btext= false;
				boolean btextbegin=false;
				Balise baliseTitle=new Balise();
				Balise baliseText=new Balise();

				FileOutputStream fichOut=new FileOutputStream("/home/dupre/Programmation/M2/AccesNumerique/TP1/output11.xml",true);
				XMLOutputFactory output = XMLOutputFactory.newInstance();
				XMLStreamWriter writer = output.createXMLStreamWriter(fichOut);
				public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{
					if (qName.equalsIgnoreCase("title")) {
						btitle = true;
					}
					if (qName.equalsIgnoreCase("text")) {
						btext = true;
					}
					if (qName.equalsIgnoreCase("root")) {
						try {
							writer.writeStartElement("root");
						} catch (XMLStreamException e) {
							e.printStackTrace();
						}
					}
				}

				public void endElement(String uri, String localName,String qName) throws SAXException {
					if (qName.equalsIgnoreCase("root")) {
						try {
							writer.writeEndElement();
						} catch (XMLStreamException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (qName.equalsIgnoreCase("title")) {
						btitle = false;
					}
					if (qName.equalsIgnoreCase("text")) {
						btext = false;
						btextbegin=false;
						baliseText.cleanContent();
						try {
							writer.writeStartElement("page");

							writer.writeCharacters("\n\t");

							writer.writeStartElement(baliseTitle.getTag());
							writer.writeCharacters(baliseTitle.getContent());
							writer.writeEndElement();

							writer.writeCharacters("\n\t");

							writer.writeStartElement(baliseText.getTag());
							writer.writeCharacters("\n\t");
							writer.writeCharacters(baliseText.getContent());
							writer.writeCharacters("\n\t");
							writer.writeEndElement();
							writer.writeCharacters("\n");

							writer.writeEndElement();

						} catch (XMLStreamException e) {
							e.printStackTrace();
						}        
					}
				}

				public void characters(char ch[], int start,
						int length) throws SAXException {

					if (btitle) {
						baliseTitle.changeBalise("title",new String(ch, start, length));		
					}

					if (btext) {
						if(btextbegin) {
							baliseText.modifyContent(new String(ch, start, length));
						}
						else {
							baliseText.changeBalise("text",new String(ch, start, length));
							btextbegin=true;
						}
					}
				}

			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void parse(Path p) {
		try {
			if(!p.getFileName().toString().equals("articleX")){
				System.out.println("Début : "+p.getFileName());
				saxParser.parse(new FileInputStream(p.toString()), handler);
				System.out.println("Fin : "+p.getFileName());
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
