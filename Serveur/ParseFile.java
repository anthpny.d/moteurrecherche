import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseFile {

	public static DefaultHandler handler;
	public static SAXParser saxParser;
	public static ArrayList<String> mots;
	public static HashMap<Integer,HashMap<Integer,Double>> relationMotPages;

	public static int currentMot;
	public static int currentPage;

	public static boolean motInit;
	public static boolean pageInit;

	public static void ParseFileInit(ArrayList<String> listeMots,HashMap<Integer,HashMap<Integer,Double>> relation) {
		try {
			mots=listeMots;
			relationMotPages=relation;
			motInit=pageInit=false;
			SAXParserFactory spfactory = SAXParserFactory.newInstance();
			saxParser = spfactory.newSAXParser();
			handler = new DefaultHandler() {

				public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{
					if (qName.equalsIgnoreCase("mot")) {
						currentMot = mots.indexOf(attributes.getValue("id"));
						motInit=true;
					}
					else if (qName.equalsIgnoreCase("page")) {
						currentPage = Integer.parseInt(attributes.getValue("id"));
						pageInit=true;
					}
				}

				public void endElement(String uri, String localName,String qName) throws SAXException {
					if (qName.equalsIgnoreCase("mot")) {
						motInit=false;
					}
					else if (qName.equalsIgnoreCase("page")) {
						pageInit=false;
					}
				}

				public void characters(char ch[], int start,int length) throws SAXException {
					if(motInit && pageInit) {
						if(!relationMotPages.containsKey(currentMot)) {
							relationMotPages.put(currentMot, new HashMap<>());
						}
						relationMotPages.get(currentMot).put(currentPage, Double.parseDouble(new String(ch, start, length)));
					}
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void parse(Path p) {
		try {
			if(!p.getFileName().toString().equals("output")){
				System.out.println("Début : "+p.getFileName());
				saxParser.parse(new FileInputStream(p.toString()), handler);
				System.out.println("Fin : "+p.getFileName());
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
