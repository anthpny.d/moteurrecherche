import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ParseFileArticles {

	public static DefaultHandler handler;
	public static SAXParser saxParser;

	public static String contenu;

	public static String titleArticle;
	public static boolean goodArticle;
	public static boolean inTitle;
	public static boolean inText;
	
	public static void ParseFileInit(String title) {
		try {
			contenu="";
			titleArticle=title;
			goodArticle=inText=inTitle=false;
			SAXParserFactory spfactory = SAXParserFactory.newInstance();
			saxParser = spfactory.newSAXParser();
			handler = new DefaultHandler() {

				public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{
					if (qName.equalsIgnoreCase("title")) {
						inTitle=true;

					}
					else if (qName.equalsIgnoreCase("text")) {
						inText=true;
					}
				}

				public void endElement(String uri, String localName,String qName) throws SAXException {
					if (qName.equalsIgnoreCase("title")) {
						inTitle=false;
					}
					else if (qName.equalsIgnoreCase("text")) {
						inText=false;
						goodArticle=false;
					}
				}

				public void characters(char ch[], int start,int length) throws SAXException {
					String s=new String(ch, start, length);;
					if(inTitle && s.equals(titleArticle)) {
						goodArticle=true;
					}
					else if(inText && goodArticle) {
						contenu+=s;
					}
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void parse(Path p) {
		try {
			if(!p.getFileName().toString().equals("output")){
				System.out.println("Début : "+p.getFileName());
				saxParser.parse(new FileInputStream(p.toString()), handler);
				System.out.println("Fin : "+p.getFileName());
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}
