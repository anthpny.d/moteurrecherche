import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

public class Main{
	public static void main(String[] args) throws IOException {

		final int nbArticles=171182;

		String strCurrentLine;
		HashMap<String,Double> dicoMotsIDF=new HashMap<>();
		ArrayList<String> listeMots=new ArrayList<>();
		HashMap<Integer,String> dicoArticles=new HashMap<>();
		HashMap<Integer,HashMap<Integer,Double>> relationMotPage=new HashMap<>();
		double[] pageRank=new double[nbArticles];
		String[]tabInit;

		//Affichage contenu
		ArrayList<Integer> borneSupInt=new ArrayList<>();
		ArrayList<String> listeFich=new ArrayList<>();

		BufferedReader bufferedreaderSommaire = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/sommaire.txt"));
		while ((strCurrentLine = bufferedreaderSommaire.readLine()) != null) {
			tabInit=strCurrentLine.split(":");
			listeFich.add(tabInit[0]);
			tabInit=tabInit[1].split("-");
			borneSupInt.add(Integer.parseInt(tabInit[1]));
		}
		bufferedreaderSommaire.close();
		//******************

		System.out.println("Initialisation dico de mots");
		BufferedReader bufferedreaderDicoMots = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/dico20000.txt"));
		while ((strCurrentLine = bufferedreaderDicoMots.readLine()) != null) {
			tabInit=strCurrentLine.split(":");
			dicoMotsIDF.put(tabInit[0], Double.parseDouble(tabInit[1]));
			listeMots.add(tabInit[0]);
		}
		System.out.println("Fin initialisation dico de mots");
		bufferedreaderDicoMots.close();

		System.out.println("Initialisation pagerank");
		int i=0;
		BufferedReader bufferedreaderPageRank = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/MatricePi50.txt"));
		while ((strCurrentLine = bufferedreaderPageRank.readLine()) != null) {
			pageRank[i]=Double.parseDouble(strCurrentLine);
			i++;
		}
		System.out.println("Fin initialisation pagerank");
		bufferedreaderPageRank.close();

		System.out.println("Initialisation dico d'articles");
		BufferedReader bufferedreaderDicoArticles = new BufferedReader(new FileReader("/home/dupre/Programmation/M2/AccesNumerique/TP1/dictionnaire.txt"));
		while ((strCurrentLine = bufferedreaderDicoArticles.readLine()) != null) {
			tabInit=strCurrentLine.split(":");
			dicoArticles.put(Integer.parseInt(tabInit[0]), tabInit[1]);
		}
		System.out.println("Fin initialisation dico d'articles");
		bufferedreaderDicoArticles.close();

		System.out.println("Initialisation relation mot-pages");
		ParseFile.ParseFileInit(listeMots,relationMotPage);
		try (Stream<Path> paths = Files.walk(Paths.get("/home/dupre/Programmation/M2/AccesNumerique/TP1/RelationMotsPages100.xml"))) {
			paths.forEach(ParseFile::parse);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Fin initialisation relation mot-pages");

		int port = 8081;
		ServerSocket serverSocket = new ServerSocket(port);
		System.err.println("Server is running on port: "+port);
		while(true){
			Socket clientSocket = serverSocket.accept();
			System.err.println("Client connected");
			OutputStream clientOutput = clientSocket.getOutputStream();
			clientOutput.write("HTTP/1.1 200 OK\r\n".getBytes());
			clientOutput.write("\r\n".getBytes());
			clientOutput.write("<!DOCTYPE html>".getBytes());
			clientOutput.write("<html>".getBytes());
			clientOutput.write("<head><meta charset=\"UTF-8\"/></head>".getBytes());
			clientOutput.write("<body>".getBytes());
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String s;
			s = in.readLine();
			String []tab=s.split(" ");
			if(tab[1].contains("search")) {
				ArrayList<String> motsRecherche=new ArrayList<>();
				tab=tab[1].split("=");
				clientOutput.write("<b>Vous cherchez : </b></br>".getBytes());
				while(tab[1].indexOf("+")!=-1) {
					String motCherche=tab[1].substring(0, tab[1].indexOf("+"));
					tab[1]=tab[1].substring(tab[1].indexOf("+")+1);
					if(dicoMotsIDF.containsKey(motCherche)) {
						String rech="<p>"+motCherche+" "+relationMotPage.get(listeMots.indexOf(motCherche)).size()+"</p>";
						clientOutput.write(rech.getBytes());
						motsRecherche.add(motCherche);
					}
				}
				String motCherche=tab[1];
				if(dicoMotsIDF.containsKey(motCherche)) {
					String rech="<p>"+motCherche+" "+relationMotPage.get(listeMots.indexOf(motCherche)).size()+"</p>";
					clientOutput.write(rech.getBytes());
					motsRecherche.add(motCherche);
				}
				if(motsRecherche.size()>0) {
					ArrayList<Integer> listePage=new ArrayList<>();
					for (Map.Entry<Integer,Double> mapentry : relationMotPage.get(listeMots.indexOf(motsRecherche.get(0))).entrySet()) {
						listePage.add(mapentry.getKey());
					}
					for(int j=1;j<motsRecherche.size();j++) {
						ArrayList<Integer> communPage=new ArrayList<>();
						for (Map.Entry<Integer,Double> mapentry : relationMotPage.get(listeMots.indexOf(motsRecherche.get(j))).entrySet()) {
							if(listePage.contains(mapentry.getKey())) {
								communPage.add(mapentry.getKey());
							}
						}
						listePage=communPage;
					}
					HashMap<Integer,Double> dicoPageMatch=new HashMap<>();
					for(Integer page : listePage) {
						dicoPageMatch.put(page,0.);
					}
					double vecteurRequete=0;
					double coeffAlpha=0.5;
					double coeffBeta=1-coeffAlpha;
					int id;
					for(String mot : motsRecherche) {
						id=listeMots.indexOf(mot);
						for(Map.Entry<Integer,Double> mapentry : relationMotPage.get(id).entrySet()) {
							if(dicoPageMatch.containsKey(mapentry.getKey())) {
								dicoPageMatch.replace(mapentry.getKey(),(mapentry.getValue()+dicoPageMatch.get(mapentry.getKey())));
							}
						}
						vecteurRequete+=Math.pow(dicoMotsIDF.get(mot),2);
					}
					vecteurRequete=Math.sqrt(vecteurRequete);
					for(Map.Entry<Integer,Double> mapentry : dicoPageMatch.entrySet()) {
						dicoPageMatch.replace(mapentry.getKey(),coeffAlpha*(mapentry.getValue()/vecteurRequete));
						dicoPageMatch.replace(mapentry.getKey(),(mapentry.getValue()+(coeffBeta*pageRank[mapentry.getKey()])));
					}
					Set<Entry<Integer,Double>> entrySet = dicoPageMatch.entrySet();
					List<Entry<Integer,Double>> list = new ArrayList<>(entrySet);

					Collections.sort(list, (o1, o2) -> o1.getValue().compareTo(o2.getValue()));

					for(int p=0;p<list.size();p++) {
						String titre=dicoArticles.get(list.get(p).getKey());
						String URL="<p>"+(list.get(p).getKey())+"<a href=\"https://fr.wikipedia.org/wiki/"+titre.replace(" ","_")+"\">"+titre+"</a></p>";
						clientOutput.write(URL.getBytes());
						if(p==0) {
							ParseFileArticles.ParseFileInit(titre);
							String pathFile="/home/dupre/Programmation/M2/AccesNumerique/TP1/output/"+listeFich.get(findArticle(borneSupInt,list.get(p).getKey()));
							System.out.println(pathFile);
							try (Stream<Path> paths = Files.walk(Paths.get(pathFile))) {
								paths.forEach(ParseFileArticles::parse);
							} catch (IOException e) {
								e.printStackTrace();
							}
							for(String mot : motsRecherche) {
								ParseFileArticles.contenu=ParseFileArticles.contenu.replace(mot, "<b>"+mot+"</b>");
							}
							clientOutput.write(ParseFileArticles.contenu.getBytes());
						}
					}
				}
				else {
					clientOutput.write("Aucun resultat".getBytes());
				}
			}
			else {
				clientOutput.write("<form action=\"\" method=\"get\" class=\"form-example\">".getBytes());
				clientOutput.write("<b>Saisir votre recherche</b></br>".getBytes());
				clientOutput.write("<input id=\"searchbar\" type=\"text\" name=\"search\">".getBytes());
				clientOutput.write("<input type=\"submit\" value=\"Valider\">".getBytes());
				clientOutput.write("</form>".getBytes());
			}
			clientOutput.write("</body>".getBytes());
			clientOutput.write("</html>".getBytes());
			clientOutput.write("\r\n\r\n".getBytes());
			clientOutput.flush();
			System.out.println("Client connection closed!");
			in.close();
			clientOutput.close();
		}
	}

	public static int findArticle(ArrayList<Integer> listBorne,Integer numArticle) {
		int i=0;
		while(i<listBorne.size()) {
			if(numArticle<listBorne.get(i)) {
				return i;
			}
			i++;
		}
		return -1;
	}
}