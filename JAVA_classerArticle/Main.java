import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main {

	public static int id;
	public static BufferedWriter bw;
	public static BufferedWriter bwSommaire;
	public static SAXParser saxParser;
	public static DefaultHandler handler;

	public static void main(String [] args) {
		ParseFileInit();
		id=0;
		File file = new File("/home/dupre/Programmation/M2/AccesNumerique/TP1/test.txt");
		File fileSommaire = new File("/home/dupre/Programmation/M2/AccesNumerique/TP1/testSommaire.txt");
		try {
			if(!file.exists()) {
				file.createNewFile();
			}
			if(!fileSommaire.exists()) {
				fileSommaire.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			FileWriter fwSommaire = new FileWriter(fileSommaire.getAbsoluteFile());
			bw = new BufferedWriter(fw);
			bwSommaire = new BufferedWriter(fwSommaire);
			try (Stream<Path> paths = Files.walk(Paths.get("/home/dupre/Programmation/M2/AccesNumerique/TP1/output"))) {
				paths.forEach(Main::ParseFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			bw.close();
			bwSommaire.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void ParseFile(Path p) {
		if(!p.getFileName().toString().equals("output")){
			int debutId=id;
			int finId;
			String insert=p.getFileName().toString();
			System.out.println(insert);
			try {
				saxParser.parse(new FileInputStream(p.toString()), handler);
				finId=id;
				insert+=":";
				insert+=debutId;
				insert+="-";
				insert+=finId-1;
				insert+="\n";
				bwSommaire.write(insert);
			} catch (SAXException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void ParseFileInit() {
		try {
			//Obtenir la configuration du sax parser
			SAXParserFactory spfactory = SAXParserFactory.newInstance();
			//Obtenir une instance de l'objet parser
			saxParser = spfactory.newSAXParser();
			handler = new DefaultHandler() {

				boolean btitle = false;
				String title="";
				String insert="";
				public void startElement(String uri, String localName,String qName,Attributes attributes) throws SAXException{
					if (qName.equalsIgnoreCase("title")) {
						btitle = true;
					}
				}

				public void endElement(String uri, String localName,String qName) throws SAXException {
					if (qName.equalsIgnoreCase("title")) {
						btitle = false;
						insert+=id;
						insert+=":";
						insert+=title;
						insert+="\n";

						try {
							bw.write(insert);
						} catch (IOException e) {
							e.printStackTrace();
						}

						insert="";
						id++;
					}
				}

				public void characters(char ch[], int start,int length) throws SAXException {
					if(btitle) {
						title=new String(ch, start, length);
					}
				}

			};
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
